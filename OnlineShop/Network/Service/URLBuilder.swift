import UIKit

enum CatalogCategories: String {
    case electronics = "electronics"
    case jewelery = "jewelery"
    case mensClothing = "men's clothing"
    case womensClothing = "women's clothing"
}

enum Sorting: String {
    case desc = "desc"
    case asc = "asc"
    case none = ""
}

final class URLBuilder {

    private var baseProtocol = "https"
    private var host: String?
    private var path = ""
    private var queryItems: [String: String]?

    private func with(baseProtocol: String) -> URLBuilder {
        self.baseProtocol = baseProtocol
        return self
    }

    private func with(host: String) -> URLBuilder {
        self.host = host
        return self
    }

    private func with(path: String) -> URLBuilder {
        self.path = path
        return self
    }

    private func with(queryItems: [String: String]) -> URLBuilder {
        self.queryItems = queryItems
        return self
    }

}

extension URLBuilder {

    private func build() -> URL {

        var urlComponents = URLComponents()
        urlComponents.scheme = baseProtocol
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = queryItems?.map {
            URLQueryItem(name: $0, value: $1)
        }
        return urlComponents.url ?? URL(string: "")!
    }
}
extension URLBuilder {
    func getCatalog(sort: Sorting) -> URL {
        let catalogURL = URLBuilder()
            .with(baseProtocol: Constants.baseProtocol)
            .with(host: Constants.baseURL)
            .with(path: "/products")
        if sort != .none {
            catalogURL.with(queryItems: ["sort": sort.rawValue])
        }
        return catalogURL.build()
    }
}
