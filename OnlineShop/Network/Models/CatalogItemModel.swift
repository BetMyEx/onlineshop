import Foundation
struct CatalogItem: Hashable, Codable {
    let id: Int
    let title: String?
    let price: Double?
    let image: String?

    enum CodingKeys: String, CodingKey, Hashable {
        case title, price, id
        case image
    }
}
struct ItemData: Decodable, Hashable {
    let data: [CatalogItem]
}
