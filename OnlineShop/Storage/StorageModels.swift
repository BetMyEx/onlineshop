import RealmSwift
class UpdateInfo: Object {
    @Persisted(primaryKey: true) var url = ""
    @Persisted var time = Date()
}
class CatalogItemCache: Object {
    @Persisted var id = 0
    @Persisted var title = ""
    @Persisted var price: Double = 0
    @Persisted var image = ""
}
class CatalogListCache: Object {
    @Persisted(primaryKey: true) var url = ""
    @Persisted var list = List<CatalogItemCache>()
}
