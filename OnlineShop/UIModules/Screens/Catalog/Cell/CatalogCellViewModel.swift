import UIKit
import Combine
final class CatalogCellViewModel {
    var image: String = ""
    var name: String = ""
    var price: Double = 0
    private let item: CatalogItem
    init(item: CatalogItem) {
        self.item = item
        setUpBindings()
    }
    private func setUpBindings() {
        self.image = item.image ?? "not found"
        self.name = item.title ?? "not found"
        self.price = item.price ?? 0
    }
}
