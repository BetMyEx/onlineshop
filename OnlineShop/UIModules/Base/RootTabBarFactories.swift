import UIKit

enum TabsType {
    case catalog
    case favorites
    case basket
    case profile
}
struct TabItem: TabItemProtocol {
    var title: String
    var controller: UIViewController
    var image: String
    var order: Int
    func toNavigationController() -> UINavigationController {
        let tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: image),
            tag: order
        )
        let viewController = controller
        viewController.tabBarItem = tabBarItem
        viewController.title = title
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.prefersLargeTitles = true
        return navigationController
    }
}
class TabItemFactory: TabItemFactoryProtocol {
    func createTabItem(for type: TabsType) -> TabItem {
        switch type {
        case .catalog:
            return TabItem(title: "Catalog", controller: CatalogViewController(), image: "catalog", order: 0)
        case .favorites:
            return TabItem(title: "Favorites", controller: FavoritesViewController(), image: "favorites", order: 1)
        case .basket:
            return TabItem(title: "Basket", controller: BasketViewController(), image: "basket", order: 2)
        case .profile:
            return TabItem(title: "Profile", controller: ProfileViewController(), image: "profile", order: 3)
        }
    }
}
