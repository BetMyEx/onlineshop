import Foundation
final class CatalogViewModel {
    private var networkClient = NetworkClientImp()
    private let urlBuilder = URLBuilder()
    @Published private(set) var items: [CatalogItem] = []
    enum Section { case mainCatalog }
    func getList() {
        let url = urlBuilder.getCatalog(sort: .asc)
        if StorageManager.checkNeedForUpdate(url: url.absoluteString) {
            networkClient.get(url: url) { [weak self] (model: [CatalogItem]) in
                self?.items = model
                DispatchQueue.main.async {
                    let catalogToCache = CatalogListCache()
                    catalogToCache.url = url.absoluteString
                    for item in self!.items {
                        catalogToCache.list.append(StorageManager.toCacheElement(item: item))
                    }
                    StorageManager.saveCatalogItemList(list: catalogToCache)
                    StorageManager.createUpdateInfo(url: url.absoluteString)
                }
            }
        } else {
            let listFromCache = StorageManager.getCatalogList(url: url.absoluteString)
            var arrayFromCache = [CatalogItem]()
            for item in listFromCache.list {
                arrayFromCache.append(StorageManager.fromCacheElement(item: item))
                print(item.title)
            }
            self.items = arrayFromCache
        }
    }
}
