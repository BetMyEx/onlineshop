import UIKit
import PinLayout
final class CatalogCell: UICollectionViewCell {

    private var networkClient = NetworkClientImp()
    static let identifier = "CatalogCell"
    var viewModel: CatalogCellViewModel! {
        didSet { setUpViewModel() }
    }

    lazy var imageView = UIImageView()
    lazy var nameLabel = UILabel()
    lazy var priceLabel = UILabel()
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setUpConstraints()
        addSubviews()
        self.layer.cornerRadius = 10
        self.backgroundColor = .lightGray
    }
    override func layoutSubviews() {
        setUpConstraints()
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
    }
    func setUpViewModel() {
        networkClient.downloadImage(url: URL(string: viewModel.image)!, completion: { image in
            self.imageView.image = image
        })
        nameLabel.text = viewModel.name
        priceLabel.text = "\(viewModel.price)"
    }
    func addSubviews() {
        let subviews = [imageView, nameLabel, priceLabel]
        subviews.forEach { view in
            contentView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    func setUpConstraints() {
        imageView.pin.horizontally(5).top(2%).bottom(30%)
        nameLabel.pin.below(of: imageView).horizontally(5).bottom(15%)
        priceLabel.pin.below(of: nameLabel).horizontally(5).bottom()
    }
    override func prepareForReuse() {
        self.imageView.image = nil
    }
}
