import UIKit

final class RootTabBarController: UITabBarController {
    private var tabItemFactory = TabItemFactory()
    private lazy var elements = [tabItemFactory.createTabItem(for: .catalog),
                                 tabItemFactory.createTabItem(for: .favorites),
                                 tabItemFactory.createTabItem(for: .basket),
                                 tabItemFactory.createTabItem(for: .profile)
    ]
    func setUpNavigationControllers() {
        let tabItems = elements
            .sorted { $0.order < $1.order }
            .compactMap { $0.toNavigationController() }
        viewControllers = tabItems
    }
}
