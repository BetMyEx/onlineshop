import UIKit
import RealmSwift

let realm = try! Realm()

final class StorageManager {
    static func checkNeedForUpdate(url: String) -> Bool {
        guard let updateInfo = realm.object(ofType: UpdateInfo.self, forPrimaryKey: url) else { return true }
        if (updateInfo.time.distance(to: Date.now)) > 30 {
            return true
        } else {
            return false
        }
    }
    
    static func createUpdateInfo(url: String) {
        let updateInfo = UpdateInfo()
        updateInfo.url = url
        updateInfo.time = Date()
        try! realm.write({
            realm.add(updateInfo, update: .modified)
        })
    }
    
    static func saveCatalogItemList(list: CatalogListCache) {
        guard let existingList = realm.object(ofType: CatalogListCache.self, forPrimaryKey: list.url) else {
            try! realm.write {
                realm.add(list)
            }
            return
        }
        try! realm.write {
            realm.delete(existingList.list)
            existingList.list = list.list
        }
    }

    static func getCatalogList(url: String) -> CatalogListCache {
        guard let list = realm.object(ofType: CatalogListCache.self, forPrimaryKey: url) else { return CatalogListCache() }
        return list
    }
    
    static func addItemToCacheCatalog(itemList: CatalogListCache, item: CatalogItemCache) {
        try! realm.write {
            itemList.list.append(item)
        }
    }
    
    static func deleteCatalogList(itemList: CatalogListCache) {
        try! realm.write({
            let items = itemList.list
            realm.delete(items)
            realm.delete(itemList)
        })
    }
    
    static func fromCacheElement(item: CatalogItemCache) -> CatalogItem {
        return CatalogItem(id: item.id, title: item.title, price: item.price, image: item.image)
    }
    
    static func toCacheElement(item: CatalogItem) -> CatalogItemCache {
        let cacheItem = CatalogItemCache()
        cacheItem.id = item.id
        cacheItem.title = item.title ?? "no data"
        cacheItem.price = item.price ?? 0
        cacheItem.image = item.image ?? "no data"
        return cacheItem
    }
}
