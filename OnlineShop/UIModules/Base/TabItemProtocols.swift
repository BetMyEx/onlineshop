import UIKit

protocol TabItemProtocol {
    var title: String { get }
    var controller: UIViewController { get }
    var image: String { get }
    var order: Int { get }
    func toNavigationController() -> UINavigationController
}
protocol TabItemFactoryProtocol {
    func createTabItem(for type: TabsType) -> TabItem
}
