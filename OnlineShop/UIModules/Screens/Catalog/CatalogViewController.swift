import UIKit
import Combine
import CoreData
class CatalogViewController: UIViewController {
    typealias DataSource = UICollectionViewDiffableDataSource<CatalogViewModel.Section, CatalogItem>
    typealias Snapshot = NSDiffableDataSourceSnapshot<CatalogViewModel.Section, CatalogItem>
    private let viewModel: CatalogViewModel
    private var dataSource: DataSource!
    private var bindings = Set<AnyCancellable>()
    var context: NSManagedObjectContext!
    let catalogView = CatalogView().collectionView
    init(viewModel: CatalogViewModel = CatalogViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
        setUpDataSource()
        setUpBindings()
        print("???")
        viewModel.getList()
    }
    override func loadView() {
        view = catalogView
    }
    func setUpCollectionView() {
        catalogView.register(CatalogCell.self, forCellWithReuseIdentifier: CatalogCell.identifier)
        catalogView.alwaysBounceVertical = true
    }
    func setUpBindings() {
        func bindViewModelToView() {
            viewModel.$items
                .receive(on: RunLoop.main)
                .sink { [weak self] _ in
                    self?.updateSections()
                }
                .store(in: &bindings)
        }
        bindViewModelToView()
    }
    private func updateSections() {
        var snapshot = Snapshot()
        snapshot.appendSections([.mainCatalog])
        snapshot.appendItems(viewModel.items)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
}
extension CatalogViewController {
    func setUpDataSource() {
        dataSource = UICollectionViewDiffableDataSource(collectionView: catalogView,
                                                        cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CatalogCell.identifier,
                                                          for: indexPath) as? CatalogCell
            cell?.viewModel = CatalogCellViewModel(item: item)
            return cell
        })
    }
}
