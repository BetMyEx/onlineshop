import Foundation
protocol NetworkClient {
   func get<T: Decodable>(url: URL, onComplete: @escaping (T) -> Void)
}
