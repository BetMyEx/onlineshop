import Foundation
import Combine
import UIKit
final class NetworkClientImp: NetworkClient {

    var imageCache = NSCache<NSString, UIImage>()

    func get<T: Decodable>(url: URL, onComplete: @escaping (_ model: T) -> Void) {
        let urlSession = URLSession(configuration: .default)
        let task = urlSession.dataTask(with: url) { data, _, error in
            let decoder = JSONDecoder()
            if let data = data {
                do {
                    let result = try decoder.decode(T.self, from: data)
                    onComplete(result)
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }

    func downloadImage(url: URL, completion: @escaping (UIImage?) -> Void) {
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage)
        } else {
            let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 10)
            let dataTask = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in

                guard error == nil,
                    data != nil,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let `self` = self else {
                        return
                }

                guard let image = UIImage(data: data!) else { return }
                self.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                DispatchQueue.main.async {
                    completion(image)
                }
            }
            dataTask.resume()
        }
    }
 }
